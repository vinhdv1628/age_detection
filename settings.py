import os

DIR_PATH = os.path.dirname(os.path.realpath(__file__))
DATA_TRAIN_PATH = os.path.join(DIR_PATH, 'data/train/')
DATA_TEST_PATH = os.path.join(DIR_PATH, 'data/test/')
DATA_TRAIN_JSON = os.path.join(DIR_PATH, 'data_train.json')
DATA_TEST_JSON = os.path.join(DIR_PATH, 'data_test.json')
STOP_WORDS = os.path.join(DIR_PATH, 'stopwords_nlp_vi.txt')
SPECIAL_CHARACTER = '%@$.,=+-!;/()*"&^:#|\n\t\''
DICTIONARY_PATH = 'dictionary.txt'

